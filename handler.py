import constants
from random import choice, random
import re
import util

VENT = re.compile(r'^vent (.*)', re.I)

def get_message_to_all(sanitised_message):
    rs = constants.RESPOND_ALL.get(sanitised_message)
    if rs:
        return choice(rs)

def get_message_to_me(sanitised_message):
    m = VENT.match(sanitised_message)
    if m is not None:
        vent = m.groups()
        return "'" + str(vent[0]) + "' has been logged with /dev/null"

    if sanitised_message in constants.ONE_WAY:
        return choice(constants.ONE_WAY[sanitised_message])

    if sanitised_message in constants.CALLABLES:
        return constants.CALLABLES[sanitised_message]()

    for regex, responses in constants.REGEXES.items():
        if re.search(regex, sanitised_message):
            return choice(responses)

    for greetings in constants.GREETINGS:
        for i, greeting in enumerate(greetings):
            if sanitised_message == util.sanitise(greeting):
                return choice(
                    greetings[:i] + greetings[i + 1:]
                )

    for query, responses in constants.INCLUDES.items():
        if query in sanitised_message:
            return choice(responses)
    
