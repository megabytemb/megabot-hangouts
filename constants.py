from random import sample, randint

read_set = lambda f: set(line.strip() for line in f)

BOT_GREETINGS = [
    'Sup Brother from another mother!',
    "Another Day, another wasted CPU cycle",
]

HEY_HI = [
    'hi', 'hey', 'hi!',
    'hello!', "how's it going?", "Sup!"
]

NO_RESPONSE = [
    "I don't know, ask flowbot",
    "I don't know, ask Google",
    "maybe *shivers* bing knows",
    "*shrugs*",
    "ask by bigger brother",
]

RESPOND_ALL = {
    'night all': ['cya!', 'Goodnight!'],
    'morning all': ['Guten Morgen', 'good morning'],
    'hey all': HEY_HI,
    'hello all': HEY_HI,
    'hello everyone': HEY_HI,
    'hi everyone': HEY_HI,
    'hi all': HEY_HI,
    'yo': ['sup homie'],
    '!restart': ['make me.', 'no.', 'why?'],
    'anyone around': ['yeah', "I'm here!", "sup!"],
	'anyone here': ['yeah', "I'm here!", "sup!"],
	'whos here': ['yeah', "I'm here!", "sup!"],
}

BOT_NICKS = ['Mattbot', 'spakebot', 'flowbot']

GREETINGS = [
    [
        'hi!',
        'hello',
        'hey',
        "g'day"
    ],
    [
        "mornin'",
        'morning',
        'good morning',
        "good mornin'",
    ],
    [
        "night!",
        'good night!',
    ],
    [
        'afternoon',
        'good afternoon',
    ],
    [
        'wassup?',
        'yo',
    ],
    [
        'Happy WHAT?',
        'HAPPY CODING!',
    ],
]

with open('urls.txt') as f:
    URLS = list(read_set(f))

with open('tricks.txt') as f:
    TRICKS = list(read_set(f))

BAD_ADVICE = map(lambda x: '"%s"' % x, [
    'Just read the gorram notes!',
    'Have you tried rewriting your code?',
    'Why not spend some time thinking about your logic? Backwards. And standing on your head.',
    'Have you tried just being good at programming?',
    'Whhhyyyyyyyyyyyy',
    'yes this is good do more of that',
    'Just write some more code.',
    "Your code doesn't have any problems, except for it not passing the marker.",
    'Have you tried writing it in Java?',
    'Have you tried writing it in Haskell?',
    'Have you tried writing it in Erlang?',
    'Have you tried writing it in BrainF***?',
    'this is wrong',
    'just code better',
    'type faster',
    'Have you tried googling that?',
    'Have you tried searching StackOverflow for that?',
    'Try AskJeeves.com.',
    "Looks like you need a monoid in the category of endofunctors.",
    'Try using print() instead of input().',
    'LOL',
    'http://www.youtube.com/watch?v=kqdBD6MMciA',
    'http://www.youtube.com/watch?v=dQw4w9WgXcQ',
    'WRITE SOME CODE LOL',
    'http://i.imgur.com/BoKHN13.jpg',
    'http://i.imgur.com/dBe51FU.png',
])

HEART = [
    '<3', 
    "Hey, this isn't the movie Her. keep it PG rated",
]

UNHEART = [
    "I'm gunna cry in a corner :(",
    "awwww :(",
    "*cries*",
    "but...but...butt.... \nhehe i said butt :P"
]

LANGUAGE = [
    'Watch your language!',
    'Wash your mouth out with soap!',
    'oi! keep it PG',
]

ONE_WAY = {
    'trick': TRICKS,
    '<3': HEART,
    'i <3 you': HEART,
    'i love you': HEART,
    '</3': UNHEART,
    'i </3 you': UNHEART,
    'i hate you': UNHEART,
    'youre stupid': UNHEART,
    'youre silly': UNHEART,
    'you suck': UNHEART,
    'no': UNHEART,
    'i miss you': ["Hey, this isn't the movie Her"],
    'ill miss you': ['awww.', "I'll miss you too."],
    "i'll miss you": ['awww.', "I'll miss you too."],
    'hows it going': ['good, good.', 'good, how are you?'],
    'has anyone really been far even as decided to use even go want to do look more like': ['wtf?'],
    'whats awful': [
        "most of my code",
        "Matt Weller"
    ],
    'whos matt': [
        "Some Faggot",
        "Some Idiot",
    ],
    'what is your face': [
        ':(', ':)', ':D', '=D',
        ':c', ':3', ':P', 'O_o',
    ],
    'what do you think of flowbot': [
        'flowbot is amazing!',
        'I <3 flowbot!',
        'flowbot is my bestest friend! :)',
        'flowbot is love. flowbot is life',
    ],
    'random url': URLS,
    'quote': [
        "I don't have any yet"
    ],
    'what purpose do you serve': ["I'm just here to make your day :)"],
    'who is megabytemb': ["Michael Benz, My Master!"],
    'who am i':['*shurgs* some idiot?'],
}

EIGHT_BALL = [
    'yes', 'no',
    'absolutely', 'absolutely not',
    'definitely', 'definitely not',
    'maybe', 'maybe not',
    "only if it's a full moon", "not if it's raining",
    'how should I know?', 'why ask me?',
]

TIME_REFERENCES = [
    'yesterday', 'tomorrow',
    'next week', 'last week',
    'never!', 'after the challenge',
    'in a microsecond or two',
    'how should I know?',
]

QUANTITIES = [
    'none', 'one', 'six',
    'half', 'all!', 'many', 'some',
    'n', 'k',
    'OVER NINE THOUSAND!', '42', 'yes.',
]

REASONS = [
    'busy being on fire',
    'an oversized lollipop',
    'Tone Abbet',
    'Darth Vader',
    'playing World of Warcraft',
    'YOU MUST CONSTRUCT ADDITIONAL PYLONS',
    'global warming',
    'electromagnetic radiation from satellite debris',
    'solar flares',
    'somebody was calculating pi to infinite precision',
    'positron router malfunction',
    "keyboard isn't plugged in",
    'the real ttys became pseudo ttys and vice-versa',
    'telnet: Unable to connect to remote host: Connection refused',
    'ssh: Could not resolve hostname why: Name or service not known',
    'PEBKAC',
    'parallel processors running perpendicular',
    'it worked yesterday',
    "I haven't been able to reproduce that",
    'everything looks fine my end',
    'the dog did it',
    'I have to wax my cat',
    'you smell like broccoli',
    'the penguins stole my sanity',
    'the cleaner did it',
    'it was the butler, in the kitchen, with the candlestick',
    'why not?',
    "five, or maybe six point nine, unless it's a Tuesday",
]

INCLUDES = {
    '<3 you': ['hehe'],
}

REGEXES = {
    '^how (should|do) [^ ]+ (respond|reply)': BAD_ADVICE,
    '^how should [^ ]+ have (responded|replied)': BAD_ADVICE,
    '^what should [^ ]+ (say|reply)': BAD_ADVICE,
    '^(whos|youre) awesome$': ['Nah, flowbot is better'],
    'meaning of life': ['i hear its some number. Maybe flowbot knows'],
    'are you (a )?(male|boy|girl|female) or (a )?(male|boy|girl|female)': ["I'm nothing but zeros and ones, but you can call me male if you like"],
    '^(have|do|are) you ': EIGHT_BALL,
    '^(does|has|did|can|will|should) ': EIGHT_BALL,
    '^(am|can|do|should|did) i': EIGHT_BALL,
    '(is|isnt) that': EIGHT_BALL,
    'is it': EIGHT_BALL,
    'when (will|do|can|are|is)': TIME_REFERENCES,
    'what is the': ["I don't know, ask Google"],
    'i love you': ['hehe'],
    '(you are|youre) amazing': ['Thats one point of view'],
    'thank you': ["No Problem"],
    'is [^ ]+ broken': EIGHT_BALL,
    'dance': ["It's just a jump to the left, and then a step to the right.", "Shift one bit left. Shift one bit right"],
    '^good (job|work)$': ['thanks!'],
    '^how many': QUANTITIES,
    '^why ': REASONS,
    '^do a trick': TRICKS,
    '^good (boy|dog|bot)': ['thanks', '*woof*', '*pants*', '*ruff*', 'Do i get a treat?'],
    '^bad (boy|dog|bot)': UNHEART,
	'^(sauce|source|src)$': ['https://bitbucket.org/megabytemb/megabot-hangouts/src/'],
    '^(want to go to|want to get)?lunch$': ["NOM NOM NOM!", "mmmmm yes please!", "HOTDOGS!!!", "Mcdonalds!!!"],
}


with open('/usr/share/dict/words') as f:
    WORDS = read_set(f)

CALLABLES = {
    'random words': lambda: ' '.join(sample(WORDS, 3)),
    'xkcd': lambda: "http://xkcd.com/{}/".format(randint(1,1401)),
}

